// RGBW (Red Green Blue White Neo-Pixel starter code
// 16 LEDS
// CW Coleman 211025

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#define PIN 6
#define NUM_LEDS 16
#define BRIGHTNESS 50

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRBW + NEO_KHZ800);


void setup() {
  Serial.begin(115200);
  strip.setBrightness(BRIGHTNESS);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

// Initialize some variables for the void loop()
int i, led1,led2,led3, led4,led5, red, green, white, blue,start,theend;
int led[5] = {0,1,2,3,4,};
int wait = 100;

void loop() {
// turn on leds 
  white = 10;
  for (i = 0; i < 5; i++);{
   led[i] = random(0,16);
  start = random(100,256);
  theend = random(100,256);
  led1 = random(0,16);
  led2 = random(0,16);
  led3 = random(0,16);
  led4 = random(0,16);
  led5 = random(0,16);
  red = random(200,256);
  green = random(0,256);
  blue = random(0,127);
  //white = random(0,10);
  //white = 10;
  }
    strip.setPixelColor(led1, red, green, blue, white);
    strip.setPixelColor(led2, red, green, blue, white);
    strip.setPixelColor(led3, red, green, blue, white);
    strip.setPixelColor(led4, red, green, blue, white);
    strip.setPixelColor(led5, red, green, blue, white);
    strip.show();
   delay(wait);
   //this loop sets all leds to black
   for ( led1 = 0; led1 < 16; led1++);
    strip.setPixelColor(led1, 0,0,0,0);
  //end of for loop
    strip.show();
}
