// RGBW (Red Green Blue White Neo-Pixel starter code
// 16 LEDS with no loop inside of void loop()
// CW Coleman 180417

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 6

#define NUM_LEDS 16

#define BRIGHTNESS 50

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRBW + NEO_KHZ800);


void setup() {
  Serial.begin(115200);
  strip.setBrightness(BRIGHTNESS);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

// Initialize some variables for the void loop()
int led1, led2, red, green, blue, white;
int wait = 100;

void loop() {
// turn on led 0 - - -
  led1 = 0; 
  red = 255; 
  green = 0; 
  blue = 0; 
  white = 10; 
  strip.setPixelColor(led1, red, green , blue, white);
  delay(wait);
  strip.show();
  
// turn on the next led
  led1 = led1 +1 ; red = random(255); green = 106; blue = 250; white = 60; 
  strip.setPixelColor(led1, red, green , blue, white);
  delay(wait);
  strip.show();

// turn on the third led
led2 = led2 +2 ; red = random (100); green = random (255); blue = random (0); white = 10;
strip.setPixelColor(led2, red, green, blue, white);
delay(wait);
strip.show();
}
